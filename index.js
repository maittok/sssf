'use strict';
const express = require('express');
const app = express();
app.use('public', express.static('public'));
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
mongoose.Promise = global.Promise;

const genderEnum = {
    MALE: 1,
    FEMALE: 2,
};
const Schema = mongoose.Schema;
const catSchema = new Schema({
    name: String,
    dob: Date,
    gender: String,
    color: String,
    weight: Number,
});
const Cat = mongoose.model('Cat', catSchema);
const host = process.env.DB_HOST;
const pass = process.env.DB_PASS;
const user = process.env.DB_USER;

mongoose.connect('mongodb://mordeuser:1092zuser@mongodb11300-mortti.jelastic.metropolia.fi:27017/db').then(() => {
    console.log('Connected successfully.');
    app.listen(3000);
}, (err) => {
    console.log('Connection to db failed: ' + err);
});

app.get('/', (req, res) => {
    res.send('Hello');
});
